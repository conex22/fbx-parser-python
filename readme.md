This program will convert Ascii-FBX 6.1 Files to a custom format made up by myself,
use the example converts to see the output style.

Currently, only files exported from maya will work for some reason...


Important: 
Exceptions handing is only done when -auto flag is set as argument

Exceptions for -auto flag still missing for skinning and static mesh 
at writing and creating, extracting is left out, the data is too crude to check there

Examples of usage:

#to extract an animation
python file_with_anim.fbx -a animation1   //This results in a file called animation1.animation

#to get static mesh data only, for inanimated objects
python some-model.fbx -m staticmodel1    //Results in a staticmodel1.mesh

#to get a skinned model
python character.fbx -s char1           //results in 3 files, char1.pose, char1.deformer, char1.connections


