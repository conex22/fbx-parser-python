import unittest

from fbx_parser_blackboxtest.test_skinned_parsing import Test_Skinned_Convert
from fbx_parser_blackboxtest.test_animation_parsing import Test_Animation_Convert
from fbx_parser_blackboxtest.test_static_parsing import Test_Static_Convert


if __name__ == '__main__':
    unittest.main()
